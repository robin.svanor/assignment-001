// DOM Elements
const bankBalance = document.getElementById("bankBalance")
const loanText = document.getElementById("loanText") 
const outstandingLoan = document.getElementById("outstandingLoan")
const payBalance = document.getElementById("payBalance") 
const laptops = document.getElementById("laptops") 
const laptopTitle = document.getElementById("laptopTitle")
const laptopSpecs = document.getElementById("laptopSpecs")
const laptopsDescription = document.getElementById("laptopsDescription") 
const laptopPrice = document.getElementById("laptopPrice") 
const laptopsImage = document.getElementById("laptopsImage") 
const getALoanButton = document.getElementById("getALoanButton") 
const bankButton = document.getElementById("bankButton") 
const workButton = document.getElementById("workButton") 
const repayLoanButton = document.getElementById("repayLoanButton")
const buyNowButton = document.getElementById("buyNowButton")


let laptopsList = []
let balance = 0
let pay = 0
let loan = 0

const formatNumbers = () => {
    bankBalance.innerText = Intl.NumberFormat('no-NO', { style: 'currency', currency: 'NOK' }).format(balance)
    payBalance.innerText = Intl.NumberFormat('no-NO', { style: 'currency', currency: 'NOK' }).format(pay)
    outstandingLoan.innerText = Intl.NumberFormat('no-NO', { style: 'currency', currency: 'NOK' }).format(loan)
}

const laptopsApi = "https://noroff-komputer-store-api.herokuapp.com/"


// Fetches all laptops from the API
fetch(laptopsApi + "computers")
    .then(response => response.json())
    .then(data => laptopsList = data)
    .then(laptopsList => addLaptopsToMenu(laptopsList))


// Adds each laptop from the laptops array to the drop down menu
const addLaptopsToMenu = (laptopsList) => {
    laptopsList.forEach(x => addLaptopToMenu(x))
    laptopTitle.innerText = laptopsList[0].title
    laptopPrice.innerText = laptopsList[0].price
    laptopsDescription.innerText = laptopsList[0].description
    laptopsImage.src = laptopsApi + laptopsList[0].image
    laptopSpecs.innerText = laptopsList[0].specs.join("\r\n")
}

const addLaptopToMenu = (laptop) => {
    const laptopElement = document.createElement("option")
    laptopElement.value = laptop.id
    laptopElement.appendChild(document.createTextNode(laptop.title))
    laptops.appendChild(laptopElement)
}


// Takes the attributes from the selected laptop and displays them 
const handleLaptopMenuChange = e => {
    const selectedLaptop = laptopsList[e.target.selectedIndex]
    laptopTitle.innerText = selectedLaptop.title
    laptopPrice.innerText = selectedLaptop.price
    laptopsImage.src = laptopsApi + selectedLaptop.image
    laptopsDescription.innerText = selectedLaptop.description
    laptopSpecs.innerText = selectedLaptop.specs.join("\r\n")

}


// Handles the function of the loan button
// Checks if the loan amount is over double of your balance or if you already have a loan
const handleGetALoanButton = () => {
    const loanAmount = Number(prompt("Please enter the amount you would like to loan: "))
    if ( 
        loanAmount <= balance * 2 &&
        loan <= 0
        ) {
            balance = parseInt( balance + loanAmount )
            bankBalance.innerText = balance
            loan = parseInt( loan + loanAmount )
            outstandingLoan.innerText = loan
            loanText.style.visibility = "visible"
            repayLoanButton.style.visibility = "visible"
    }
    else {
        alert("Loan cannot be over double of your balance OR you cannot have more than one loan at a time!")
    }
    formatNumbers()
}


// Adds 100 to the pay balance every time the user clicks on the 'work' button
const handleWorkButton = () => {
    pay += 100
    payBalance.innerText = pay
    formatNumbers()
}


/*Bank button transfers pay balance to bank balance
if user has a loan 10 % of the pay balance goes towards the
downpayment on the loan, the rest goes to the bank balance */
const handleBankButton = () => {
    if ( loan > 0 ) {
        loan = parseInt( loan - pay * ( 0.1 ) )
        outstandingLoan.innerText = loan
        balance = parseInt( balance + pay * ( 0.9 ) )
        bankBalance.innerText = balance
        pay = 0
        payBalance.innerText = pay
    }
    else {
        balance = parseInt( balance + pay )
        bankBalance.innerText = balance
        pay = 0
        payBalance.innerText = pay
    }
    formatNumbers()
}


/* Repay loan button checks if user can pay back loan
if user has enough money the entire pay balance goes towards the outstanding loan
and the rest goes to the users bank balance */
const handleRepayLoanButton = () => {
    if ( pay >= loan ) {
        pay = parseInt( pay - loan )
        payBalance.innerText = pay
        loan = 0
        outstandingLoan.innerText = loan
        balance = parseInt( balance + pay )
        bankBalance.innerText = balance
        pay = 0
        payBalance.innerText = pay
    }
    else {
        alert("You dont have enough money to pay your loan, Work to make some money!")
    }
    formatNumbers()
}


// Checks if user have enough in their bank to buy the selected laptop,
// if user has enough the money gets withdrawn from their bank balance
const handleBuyNowButton = () => {
    const selectedLaptop = laptopsList[laptops.selectedIndex] 
    if ( balance >= selectedLaptop.price ) {
        alert("---!! You are now the proud owner of " + selectedLaptop.title + " !!---")
        balance = parseInt( balance - selectedLaptop.price )
        bankBalance.innerText = balance
    }
    else {
        alert("You do not have sufficient funds to purchase "+ selectedLaptop.title + ", Work to make money!")
    }
    formatNumbers()
}


// Event listeners
laptops.addEventListener("change", handleLaptopMenuChange)
getALoanButton.addEventListener("click", handleGetALoanButton)
workButton.addEventListener("click", handleWorkButton)
bankButton.addEventListener("click", handleBankButton)
repayLoanButton.addEventListener("click", handleRepayLoanButton)
buyNowButton.addEventListener("click", handleBuyNowButton)